def read_input():
    n = int(input())
    x = [int(input()) for i in range(n)]
    return n,x


def sort(x, n):
    for i in range(n-1):
        mi = i
        mn = x[i]

        for j in range(i+1,n):
            if x[j] < mn:
                mi = j
                mn = x[j]

        x[mi], x[i] = x[i], x[mi]

        
def output(x):
    for y in x:
        print(y)

        
def main():
    n,x = read_input()
    sort(x, n)
    output(x)


if __name__ == '__main__':
    main()
