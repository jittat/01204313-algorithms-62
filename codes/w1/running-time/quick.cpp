#include <cstdio>
#include <cstdlib>

#define MAX_N 5000000

int x[MAX_N];
int n;

void read_input()
{
  scanf("%d",&n);
  for(int i=0; i<n; i++)
    scanf("%d",&x[i]);
}

int intcmp(const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}


void sort()
{
  qsort(x, n, sizeof(int), intcmp);
}

void output()
{
  for(int i=0; i<n; i++)
    printf("%d\n",x[i]);
}

int main()
{
  read_input();
  sort();
  output();
}
