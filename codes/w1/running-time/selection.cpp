#include <cstdio>

#define MAX_N 5000000

int x[MAX_N];
int n;

void read_input()
{
  scanf("%d",&n);
  for(int i=0; i<n; i++)
    scanf("%d",&x[i]);
}

void sort()
{
  for(int i=0; i<n-1; i++) {
    int mn = x[i];
    int mi = i;
    for(int j=i+1; j<n; j++) {
      if(x[j] < x[mi]) {
        mi = j;
        mn = x[j];
      }
    }
    int tmp = x[i];
    x[i] = x[mi];
    x[mi] = tmp;
  }
}

void output()
{
  for(int i=0; i<n; i++)
    printf("%d\n",x[i]);
}

int main()
{
  read_input();
  sort();
  output();
}
