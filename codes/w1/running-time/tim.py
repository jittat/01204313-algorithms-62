def read_input():
    n = int(input())
    x = [int(input()) for i in range(n)]
    return n,x


def sort(x, n):
    # Python uses Timsort [https://en.wikipedia.org/wiki/Timsort]
    
    x.sort()

        
def output(x):
    for y in x:
        print(y)

        
def main():
    n,x = read_input()
    sort(x, n)
    output(x)


if __name__ == '__main__':
    main()
