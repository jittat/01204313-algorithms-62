import sys
from random import shuffle

n = int(sys.argv[1])
print(n)
nums = [x+1 for x in range(n)]
shuffle(nums)
for x in nums:
    print(x)
