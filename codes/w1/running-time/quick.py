def read_input():
    n = int(input())
    x = [int(input()) for i in range(n)]
    return n,x


def qsort(x):
    if len(x) <= 1:
        return x
    
    p = x[0]
    center = [y for y in x if y == p]
    left = [y for y in x if y < p]
    right = [y for y in x if y > p]

    return qsort(left) + center + qsort(right)
        
def output(x):
    for y in x:
        print(y)

        
def main():
    n,x = read_input()
    x = qsort(x)
    output(x)


if __name__ == '__main__':
    main()
